<html>
  <head>
     <meta charset="utf-8">
    <title>Form</title>
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:400,700&amp;subset=cyrillic-ext">
    <link rel="stylesheet" href="style.css">
    <style>
/* Сообщения об ошибках и поля с ошибками выводим с красным бордюром. */
.error {
  border: 2px solid red;
}
    </style>
  </head>
  <body>

<?php
if (!empty($messages)) {
  print('<div id="messages">');
  // Выводим все сообщения.
  foreach ($messages as $message) {
    print($message);
  }
  print('</div>');
}

// Далее выводим форму отмечая элементы с ошибками классом error
// и задавая начальные значения элементов ранее сохраненными.
?>
  <div class="main-container ">
  
    
    <div class="form">
      <form action="" method="POST">
        <div id="form-1">
          <label>
           Name :<br >
            <input name="fio" <?php if ($errors['fio']) {print 'class="error"';} ?> value="<?php print $values['fio']; ?>" placeholder="Name"  />
          </label><br>
        
          <label>
            email:<br >
            <input name="email" <?php if ($errors['email']) {print 'class="error"';} ?> value="<?php print $values['email']; ?>" placeholder="name@mail.ru" type="email" />
          </label><br>
          
   
          <label id="date">
          Date<br>
            <input name="date" <?php if ($errors['date']) {print 'class="error"';} ?> value="<?php print $values['date']; ?>" type="date"/>
          
      </label>
        </div>

        <div id="form-2">
          <div id="form-2-container">
            <div <?php if ($errors['gender']) {print 'class="error"';} ?>>
            <label > Gender:<br>
              <input type="radio"  name="radio-group-1"  <?php if ($errors['gender']and$values['gender']=="male") {print 'checked="checked"';}?> value="male" />
              Male
            </label><br>

            <label>
              <input type="radio" name="radio-group-1" <?php if ($errors['gender']and$values['gender']=="female") {print 'checked="checked"';}?> value="female" />
               Female
            </label><br>
            <br>
            </div>
            <div <?php if ($errors['limbs']) {print 'class="error"';} ?>>
           Number of limbs:<br>
            <label>
              <input type="radio" name="radio-group-2" <?php if ($errors['limbs']and$values['limbs']=="1") {print 'checked="checked"';}?> value="1" />
              1
            </label><br>

            <label>
              <input type="radio" name="radio-group-2" <?php if ($errors['limbs']and$values['limbs']=="2") {print 'checked="checked"';}?> value="2" />
              2
            </label><br>

            <label>
              <input type="radio" name="radio-group-2" <?php if ($errors['limbs']and$values['limbs']=="3") {print 'checked="checked"';}?> value="3" />
              3
            </label><br>

            <label>
              <input type="radio"  name="radio-group-2" <?php if ($errors['limbs']and$values['limbs']=="4") {print 'checked="checked"';}?> value="4" />
              4
            </label><br>

            <label>
              <input type="radio"  name="radio-group-2" <?php if ($errors['limbs']and$values['limbs']==">4") {print 'checked="checked"';}?> value=">4" />
              >4
            </label><br>
            <br>
            </div>
          </div>
        </div>
        <div id="form-3">
          <div id="form-3-container">
            <div id="super" >  
              <label >
                Your superpowers:<br>
                <select name="field-name-2"  <?php if ($errors['super']) {print 'class="error"';} ?>  multiple="multiple">
                  <option value="Telepathy" <?php if ($errors['super']and$values['super']=="Telepathy") {print 'selected="selected"';}?>>Telepathy</option>
                  <option value="Telekinesis" <?php if ($errors['super']and$values['super']=="Telekinesis") {print 'selected="selected"';}?> >Telekinesis</option>
                  <option value="Levitation" <?php if ($errors['super']and$values['super']=="Levitation") {print 'selected="selected"';}?>>Levitation</option>
                  <option value="Absolute memory" <?php if ($errors['super']and$values['super']=="Absolute memory") {print 'selected="selected"';}?> >Absolute memory</option>
                </select>
              </label><br>
           </div>

            <div id="Bio">
              <label>
                Biography:<br >
                <textarea name="bio" <?php if ($errors['bio']) {print 'class="error"';} ?> ><?php print $values['bio']; ?></textarea>
              </label><br >
            </div>
              <label <?php if ($errors['check']) {print 'class="error"';} ?>>
                <input type="checkbox" checked="checked" name="check-1"  />
                I have read the contract
              </label><br >
        
              <input type="submit" value="Submit" />
         </div>
        </div>
      </form>
    </div>
    <?php   
                    if(!empty($_SESSION['login'])){
                        print('<form method="POST" action="login.php"><input type="submit" name="exit" value="Output"></form>');
                    }
                ?>
                  
  </div>

  
  </body>
</html>