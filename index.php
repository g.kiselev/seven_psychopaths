<?php
/**
 * Реализовать проверку заполнения обязательных полей формы в предыдущей
 * с использованием Cookies, а также заполнение формы по умолчанию ранее
 * введенными значениями.
 */

// Отправляем браузеру правильную кодировку,
// файл index.php должен быть в кодировке UTF-8 без BOM.
header('Content-Type: text/html; charset=UTF-8');
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
    // В суперглобальном массиве $_SERVER PHP сохраняет некторые заголовки запроса HTTP
    // и другие сведения о клиненте и сервере, например метод текущего запроса $_SERVER['REQUEST_METHOD'].
    if (!empty($_COOKIE['save'])) {
        // Удаляем куку, указывая время устаревания в прошлом.
        setcookie('save', '', 100000);
        setcookie('login', '', 100000);
        setcookie('pass', '', 100000);
        // Выводим сообщение пользователю.
        //$messages[] = 'Thanks, the results are saved.';
        
        // Если в куках есть пароль, то выводим сообщение.
        if (!empty($_COOKIE['pass'])) {
            $messages[] = sprintf('You can <a href="login.php">login</a> with username <strong>%s</strong>
        and password <strong>%s</strong> to change informanion.',
                strip_tags($_COOKIE['login']),
                strip_tags($_COOKIE['pass']));
        }
        else{
            $messages[] = 'The results are saved. You can <a href="login.php">exit</a>.';
        }
    }
    
    // Складываем признак ошибок в массив.
    $errors = array();
    $errors['fio'] = !empty($_COOKIE['fio_error']);
    // TODO: аналогично все поля.
    $errors['email'] = !empty($_COOKIE['email_error']);
    $errors['date'] = !empty($_COOKIE['date_error']);
    $errors['gender'] = !empty($_COOKIE['gender_error']);
    $errors['limbs'] = !empty($_COOKIE['limbs_error']);
    $errors['super'] = !empty($_COOKIE['super_error']);
    $errors['bio'] = !empty($_COOKIE['bio_error']);
    $errors['check'] = !empty($_COOKIE['check_error']);
    
    // Выдаем сообщения об ошибках.
    if ($errors['fio']) {
        // Удаляем куку, указывая время устаревания в прошлом.
        setcookie('fio_error', '', 100000);
        // Выводим сообщение.
        $messages[] = '<div class="error">FIll NAME.</div>';
    }
    if ($errors['email']) {
        // Удаляем куку, указывая время устаревания в прошлом.
        setcookie('email_error', '', 100000);
        // Выводим сообщение.
        $messages[] = '<div class="error">FIll mail.</div>';
    }
    
    if ($errors['date']) {
        // Удаляем куку, указывая время устаревания в прошлом.
        setcookie('date_error', '', 100000);
        // Выводим сообщение.
        $messages[] = '<div class="error">FIll Date.</div>';
    }
    
    if ($errors['gender']) {
        // Удаляем куку, указывая время устаревания в прошлом.
        setcookie('date_error', '', 100000);
        // Выводим сообщение.
        $messages[] = '<div class="error">select to  gender.</div>';
    }
    if ($errors['limbs']) {
        // Удаляем куку, указывая время устаревания в прошлом.
        setcookie('limbs_error', '', 100000);
        // Выводим сообщение.
        $messages[] = '<div class="error">Select to limbs.</div>';
    }
    
    if ($errors['super']) {
        // Удаляем куку, указывая время устаревания в прошлом.
        setcookie('super_error', '', 100000);
        // Выводим сообщение.
        $messages[] = '<div class="error">Select to superpowers.</div>';
    }
    
    
    if ($errors['bio']) {
        // Удаляем куку, указывая время устаревания в прошлом.
        setcookie('bio_error', '', 100000);
        // Выводим сообщение.
        $messages[] = '<div class="error">fill bio.</div>';
    }
    
    
    if ($errors['check']) {
        // Удаляем куку, указывая время устаревания в прошлом.
        setcookie('check_error', '', 100000);
        // Выводим сообщение.
        $messages[] = '<div class="error">check.</div>';
    }
    
    
    // TODO: тут выдать сообщения об ошибках в других полях.
    
    // Складываем предыдущие значения полей в массив, если есть.
    $values = array();
    $values['fio'] = empty($_COOKIE['fio_value']) ? '' : $_COOKIE['fio_value'];
    $values['email'] = empty($_COOKIE['email_value']) ? '' : $_COOKIE['email_value'];
    $values['date'] = empty($_COOKIE['date_value']) ? '' : $_COOKIE['date_value'];
    $values['gender'] = empty($_COOKIE['gender_value']) ? '' : $_COOKIE['gender_value'];
    $values['limbs'] = empty($_COOKIE['limbs_value']) ? '' : $_COOKIE['limbs_value'];
    $values['super'] = empty($_COOKIE['super_value']) ? '' : $_COOKIE['super_value'];
    $values['bio'] = empty($_COOKIE['bio_value']) ? '' : $_COOKIE['bio_value'];
    $values['check'] = empty($_COOKIE['check_value']) ? '' : $_COOKIE['check_value'];
    // TODO: аналогично все поля.
    
    // Если нет предыдущих ошибок ввода, есть кука сессии, начали сессию и
    // ранее в сессию записан факт успешного логина.
    if (session_start() && !empty($_COOKIE[session_name()]) && !empty($_SESSION['login'])) {
        $user = 'u16654';
        $password = '8728536';
        $log=$_SESSION['login'];
        $db = new PDO('mysql:host=localhost;dbname=u16654', $user, $password,
            array(PDO::ATTR_PERSISTENT => true));
        try{
            $stmt = $db->prepare("SELECT name,mail,birth,sex,limbs,super,bio,check1 FROM app WHERE login = '$log'");
            $stmt->execute();
            while ($row = $stmt->fetch(PDO::FETCH_LAZY))
            {
                $values['fio']=$row['fio'];
                $values['email']=$row['email'];
                $values['date']=$row['date'];
                $values['gender']=$row['gender'];
                $values['limbs']=$row['limbs'];
                $values_f=array();
                if(!empty($row['super'])){
                    if(stristr($row['super'],'Telepathy') == TRUE) {
                        array_push($values_f,'Telepathy');
                    }
                    if(stristr($row['super'],'Telekinesis') == TRUE) {
                        array_push($values_f,'Telekinesis');
                    }
                    if(stristr($row['super'],'Levitation') == TRUE) {
                        array_push($values_f,'Levitation');
                    }
                    if(stristr($row['super'],'Absolute memory') == TRUE) {
                        array_push($values_f,'Absolute memory');
                    }
                    
                    $values['super']=serialize($values_f);
                }
                $values['bio']=$row['bio'];
                $values['check']=$row['check'];
            }
        }catch(PDOException $e){
            print('Error : ' . $e->getMessage());
            exit();
        }
    }
    // Включаем содержимое файла form.php.
    // В нем будут доступны переменные $messages, $errors и $values для вывода
    // сообщений, полей с ранее заполненными данными и признаками ошибок.
    include('form.php');
}
// Иначе, если запрос был методом POST, т.е. нужно проверить данные и сохранить их в XML-файл.
else {
    // Проверяем ошибки.
    $errors = FALSE;
    if (empty($_POST['fio'])) {
        // Выдаем куку на день с флажком об ошибке в поле fio.
        setcookie('fio_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    }
    else {
        // Сохраняем ранее введенное в форму значение на месяц.
        setcookie('fio_value', $_POST['fio'], time() + 30 * 24 * 60 * 60);
    }
    
    if (empty($_POST['email'])) {
        // Выдаем куку на день с флажком об ошибке в поле fio.
        setcookie('email_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    }
    else {
        // Сохраняем ранее введенное в форму значение на месяц.
        setcookie('email_value', $_POST['email'], time() + 30 * 24 * 60 * 60);
    }
    
    if (empty($_POST['date'])) {
        // Выдаем куку на день с флажком об ошибке в поле fio.
        setcookie('date_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    }
    else {
        // Сохраняем ранее введенное в форму значение на месяц.
        setcookie('date_value', $_POST['date'], time() + 30 * 24 * 60 * 60);
    }
    
    
    if (empty($_POST['radio-group-1'])) {
        // Выдаем куку на день с флажком об ошибке в поле fio.
        setcookie('gender_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    }
    else {
        // Сохраняем ранее введенное в форму значение на месяц.
        setcookie('gender_value', $_POST['radio-group-1'], time() + 30 * 24 * 60 * 60);
    }
    
    if (empty($_POST['radio-group-2'])) {
        // Выдаем куку на день с флажком об ошибке в поле fio.
        setcookie('limbs_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    }
    else {
        // Сохраняем ранее введенное в форму значение на месяц.
        setcookie('limbs_value', $_POST['radio-group-2'], time() + 30 * 24 * 60 * 60);
    }
    if (empty($_POST['field-name-2'])) {
        // Выдаем куку на день с флажком об ошибке в поле fio.
        setcookie('super_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    }
    else {
        // Сохраняем ранее введенное в форму значение на месяц.
        setcookie('super_value', $_POST['field-name-2'], time() + 30 * 24 * 60 * 60);
    }
    
    if (empty($_POST['bio'])) {
        // Выдаем куку на день с флажком об ошибке в поле fio.
        setcookie('bio_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    }
    else {
        // Сохраняем ранее введенное в форму значение на месяц.
        setcookie('bio_value', $_POST['bio'], time() + 30 * 24 * 60 * 60);
    }
    
    if (empty($_POST['check-1'])) {
        // Выдаем куку на день с флажком об ошибке в поле fio.
        setcookie('check_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    }
    else {
        // Сохраняем ранее введенное в форму значение на месяц.
        setcookie('check_value', $_POST['check-1'], time() + 30 * 24 * 60 * 60);
    }
    // *************
    // TODO: тут необходимо проверить правильность заполнения всех остальных полей.
    // Сохранить в Cookie признаки ошибок и значения полей.
    // *************
    
    if ($errors) {
        // При наличии ошибок перезагружаем страницу и завершаем работу скрипта.
        header('Location: index.php');
        exit();
    }
    else {
        // Удаляем Cookies с признаками ошибок.
        setcookie('fio_error', '', 100000);
        setcookie('email_error', '', 100000);
        setcookie('date_error', '', 100000);
        setcookie('gender_error', '', 100000);
        setcookie('limbs_error', '', 100000);
        setcookie('super_error', '', 100000);
        setcookie('bio_error', '', 100000);
        setcookie('check_error', '', 100000);
       
        // TODO: тут необходимо удалить остальные Cookies.
    }
    
    // Сохранение в XML-документ.
    // ...
    if (session_start() && !empty($_COOKIE[session_name()]) && !empty($_SESSION['login']))
    {
        $user = 'логин';
        $password = 'пароль';
        $super_separated='';
        $log=$_SESSION['login'];
        $db = new PDO('mysql:host=localhost;dbname=u16654', $user, $password,array(PDO::ATTR_PERSISTENT => true));
        try {
            $stmt = $db->prepare("UPDATE app SET name=?,mail=?,birth=?,sex=?,limbs=?,super=?,bio=?,check1=? WHERE login='$log' ");
            
            $name=$_POST["fio"];
            $email=$_POST["email"];
            $data=$_POST["date"];
            $gender=$_POST["radio-group-1"];
            $limbs=$_POST["radio-group-2"];
            if(!empty($_POST['field-name-2'])){
                $super_mass=$_POST['field-name-2'];
                for($w=0;$w<count($super_mass);$w++){
                    if($flag){
                        if($super_mass[$w]!="net")unset($super_mass[$w]);
                        $super_separated=implode(' ',$super_mass);
                    }else{
                        $super_separated=implode(' ',$super_mass);
                    }
                }
            }
            $super=$super_separated;
            $bio=$_POST["bio"];
            //$check1=$_POST["check-1"];
            
            $stmt->execute(array($name,$email,$data,$gender,$limbs,$super,$bio,));
        }catch(PDOException $e){
            print('Error : ' . $e->getMessage());
            exit();
        }
    }else{
        
        $logins_chars = '0123456789abcdefghijklmnopqrstuvwxyz';
        $pass_chars='0123456789';
        $login = $_POST["fio"];
        $password =substr(str_shuffle($pass_chars),0,8);
        setcookie('login', $login);
        setcookie('pass', $password);
        /*    if(!empty($_POST['field-name-2'])){
         $super_mass=$_POST['field-name-2'];
         for($w=0;$w<count($super_mass);$w++){
         if($flag){
         if($super_mass[$w]!="net")unset($super_mass[$w]);
         $super_separated=implode(' ',$super_mass);
         }else{
         $super_separated=implode(' ',$super_mass);
         }
         }
         }*/
        
        $user = 'u16654';
        $pass = '8728536';
        $db = new PDO('mysql:host=localhost;dbname=u16654', $user, $pass,
            array(PDO::ATTR_PERSISTENT => true));
        try {
            /*$stmt = $db->prepare("INSERT INTO app(name,login,password,email, data, gender, limbs,super,bio)
        VALUES (:name,:login,:password,:email, :data, :gender, :limbs,:super,:bio)");
            $stmt->bindParam(':name', $name_db);
            $stmt->bindParam(':login', $login_db);
            $stmt->bindParam(':password', $pass_db);
            $stmt->bindParam(':email', $mail_db);
            $stmt->bindParam(':data', $year_db);
            $stmt->bindParam(':gender', $sex_db);
            $stmt->bindParam(':limbs', $limb_db);
            $stmt->bindParam(':super', $super_db);
            $stmt->bindParam(':bio', $bio_db);
            //$stmt->bindParam(':check1', $check1_db);
            $name_db=$_POST["fio"];
            $login_db=$login;
            $pass_db=md5($password);
            $mail_db=$_POST["email"];
            $year_db=$_POST["date"];
            $sex_db=$_POST["radio-group-1"];
            $limb_db=$_POST["radio-group-2"];
            $super_db=$super_separated;
            $bio_db=$_POST["bio"];
            //$check1_db=$_POST["check1"];
            $stmt->execute();*/
            
            $stmt = $db->prepare("INSERT INTO application SET name = ?,login = ?,password = ?, email = ?, data = ?, gender = ?, limbs = ?, super = ?, bio = ?");
            $stmt->execute(array($_POST['fio'],$login,$password, $_POST['email'],$_POST['date'],$_POST['radio-group-1'],$_POST['radio-group-2'],
                $_POST['field-name-2'],$_POST['bio']));
        }
        catch(PDOException $e){
            print('Error : ' . $e->getMessage());
            exit();
        }
    }
    // Сохраняем куку с признаком успешного сохранения.
    setcookie('save', '1');
    
    // Делаем перенаправление.
    header('Location: index.php');
}
