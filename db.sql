CREATE TABLE application (
  id int(10) unsigned NOT NULL AUTO_INCREMENT,
  name varchar(128) NOT NULL DEFAULT '',
  login varchar(128) NOT NULL DEFAULT '',
  password varchar(128) NOT NULL DEFAULT '',
  email varchar(128) NOT NULL DEFAULT '', 
  data varchar(128) NOT NULL DEFAULT '',
  gender varchar(128) NOT NULL DEFAULT '',
  limbs varchar(128) NOT NULL DEFAULT '',
  super varchar(128) NOT NULL DEFAULT '',
  bio varchar(128) NOT NULL DEFAULT '',
  PRIMARY KEY (id)
);

